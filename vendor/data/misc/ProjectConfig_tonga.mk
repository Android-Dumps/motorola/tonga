# Only item in $(MTK_TARGET_PROJECT_FOLDER)/ProjectConfig.mk con be overlay here

#audio smartpa
MTK_AUDIO_SPEAKER_PATH = smartpa_foursemi_fs16xx

# Settings to enable and disable moto sensors
#moto algo stowed
CUSTOM_KERNEL_STOWED_SENSOR := true
#moto algo flatup
CUSTOM_KERNEL_FLATUP_SENSOR := true
#moto algo flatdown
CUSTOM_KERNEL_FLATDOWN_SENSOR := true
#moto algo camgest
CUSTOM_KERNEL_CAMGEST_SENSOR := true
#moto algo chopchop
CUSTOM_KERNEL_CHOPCHOP_SENSOR := true
#moto algo glance
CUSTOM_KERNEL_MOT_GLANCE_SENSOR := true
#moto algo ltv
CUSTOM_KERNEL_MOT_LTV_SENSOR := true
#moto algo params
CUSTOM_KERNEL_MOT_ALGO_PARAMS := true
#moto algo ftm
CUSTOM_KERNEL_MOT_FTM_SENSOR := true
#moto algo off_body
CUSTOM_KERNEL_MOT_OFFBODY_SENSOR := no
#moto algo lts
CUSTOM_KERNEL_MOT_LTS_SENSOR := true
#moto algo disprot
CUSTOM_KERNEL_MOT_DISPROT_SENSOR := true
#moto alsps
CUSTOM_KERNEL_MOT_ALSPS := true
#disable sensors not used and mtk fusion sensors
CUSTOM_KERNEL_GYROSCOPE = yes
CUSTOM_KERNEL_BAROMETER = yes
CUSTOM_KERNEL_UNCALI_ACC_SENSOR = yes
CUSTOM_KERNEL_UNCALI_GYRO_SENSOR = yes

CUSTOM_KERNEL_GLANCE_GESTURE_SENSOR = no
CUSTOM_KERNEL_PICK_UP_SENSOR = no
CUSTOM_KERNEL_TILT_DETECTOR_SENSOR = no
CUSTOM_KERNEL_WAKE_GESTURE_SENSOR = no

CUSTOM_HAL_CAMERA = camera
CUSTOM_HAL_CAM_CAL =
CUSTOM_HAL_EEPROM = dummy_eeprom
CUSTOM_HAL_FLASHLIGHT = constant_flashlight
CUSTOM_HAL_IMGSENSOR = mot_tonga_s5k4h7_mipi_raw mot_tonga_ov02b1b_mipi_raw mot_tonga_gc02m1_mipi_raw mot_tonga_s5kjn1sq_mipi_raw mot_tonga_gc02m1b_mipi_raw
CUSTOM_HAL_MAIN2_IMGSENSOR = mot_tonga_ov02b1b_mipi_raw
CUSTOM_HAL_MAIN3_IMGSENSOR = mot_tonga_gc02m1_mipi_raw
CUSTOM_HAL_MAIN_IMGSENSOR = mot_tonga_s5kjn1sq_mipi_raw
CUSTOM_HAL_SUB_IMGSENSOR = mot_tonga_s5k4h7_mipi_raw
CUSTOM_KERNEL_CAMERA = camera
CUSTOM_KERNEL_CAM_CAL =
CUSTOM_KERNEL_EEPROM = dummy_eeprom
CUSTOM_KERNEL_FLASHLIGHT = constant_flashlight
CUSTOM_KERNEL_IMGSENSOR = mot_tonga_s5k4h7_mipi_raw mot_tonga_ov02b1b_mipi_raw mot_tonga_gc02m1_mipi_raw mot_tonga_s5kjn1sq_mipi_raw mot_tonga_gc02m1b_mipi_raw
CUSTOM_KERNEL_MAIN2_IMGSENSOR =mot_tonga_ov02b1b_mipi_raw
CUSTOM_KERNEL_MAIN3_IMGSENSOR = mot_tonga_gc02m1_mipi_raw
CUSTOM_KERNEL_MAIN_BACKUP_IMGSENSOR =
CUSTOM_KERNEL_MAIN_IMGSENSOR = mot_tonga_s5kjn1sq_mipi_raw
CUSTOM_KERNEL_SUB_IMGSENSOR = mot_tonga_s5k4h7_mipi_raw
MTK_CAM_VSDOF_SUPPORT = yes
# Mot add: for nfc support
MTK_NFC_GSMA_SUPPORT = no
# Enable fingerprint
MTK_FINGERPRINT_SUPPORT = yes
MTK_FINGERPRINT_SELECT = no
# moto modem image folder
CUSTOM_MODEM=mt6762_ellis
MTK_C2K_LTE_MODE = 0
MTK_MD1_SUPPORT = 9
MTK_PROTOCOL1_RAT_CONFIG = Lf/Lt/W/G
BOOT_LOGO = moto_hdplus1600
MOTO_POWEROFF_CHARGING_UI = yes
MOTO_POWEROFF_CHARGING_UI_HDPLUS1600 = yes
MOTO_CHARGE_ONLY_MODE_ASSET_COLOR = hdplus1600

MTK_CAM_MAX_NUMBER_OF_CAMERA = 5

# Turn off MTK_TELEPHONY_ADD_ON_POLICY
MTK_TELEPHONY_ADD_ON_POLICY = 1

# According to OD, WFD is NOT supported on Saipan / Kyoto / Tonga IKSWQ-148414
MTK_WFD_SUPPORT = no

#display
LCM_HEIGHT = 1600
LCM_WIDTH = 720

MTK_AAL_SUPPORT = no

# enable VAB using mediatek default
ENABLE_VIRTUAL_AB = true

# enable globe one image for big super image in VAB
ENABLE_GLOBAL_ONEIMAGE = true

MTK_HIGH_FRAME_RATE = yes

#Overide DSBP level from MTK k65v1_64_bsp default 1 to 2.
MTK_DYNAMIC_SBP_LEVEL = 2

# MTK CXP
MTK_MULTIPLE_IMS_SUPPORT = 1
MTK_MULTI_SIM_SUPPORT = ss
MTK_VZW_DEVICE_TYPE = 3
MTK_IPV6_VZW = yes
MTK_DISABLE_CAPABILITY_SWITCH = yes
ifeq ($(MAKE_FACT),true)
MTK_CARRIEREXPRESS_PACK = no
OPTR_SPEC_SEG_DEF = NONE
MTK_MD_SBP_CUSTOM_VALUE = 0
else
MTK_CARRIEREXPRESS_PACK = na
OPTR_SPEC_SEG_DEF = NONE
MTK_MD_SBP_CUSTOM_VALUE =
endif
MTK_TEMPORARY_DATA_SUPPORT = no

#MTK rsu
SIM_ME_LOCK_MODE = 3

# Support HAC
MTK_HAC_SUPPORT = yes

#Enable MAPI (Modem Analysis Public Interface) on userdebug
ifneq ($(TARGET_BUILD_VARIANT),user)
    MTK_MAPI_SUPPORT = yes
    MSSI_MTK_MAPI_SUPPORT = yes
    MTK_DMC_SUPPORT = yes
    MTK_MODEM_MONITOR_SUPPORT = yes
endif
