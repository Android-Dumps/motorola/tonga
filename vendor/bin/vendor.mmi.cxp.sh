#!/vendor/bin/sh

# Copyright (c) 2021, Motorola Mobility LLC, All Rights Reserved.
#
# Date Created: 8/30/2021, Set CXP properties according to carrier channel
#

boot_carrier=`getprop ro.boot.carrier`

set_cxp_readonly_properties ()
{
    case $boot_carrier in
        att|attpre|cricket )
            setprop ro.vendor.mtk_md_sbp_custom_value 7
            setprop ro.vendor.operator.optr OP07
            setprop ro.vendor.operator.spec SPEC0407
            setprop ro.vendor.operator.seg SEGDEFAULT
        ;;
        tmo|boost|cc|fi|metropcs|retus|tracfone )
            setprop ro.vendor.mtk_md_sbp_custom_value 8
            setprop ro.vendor.operator.optr OP08
            setprop ro.vendor.operator.spec SPEC0200
            setprop ro.vendor.operator.seg SEGDEFAULT
        ;;
        usc )
            setprop ro.vendor.mtk_md_sbp_custom_value 236
            setprop ro.vendor.operator.optr OP236
            setprop ro.vendor.operator.spec SPEC0200
            setprop ro.vendor.operator.seg SEGDEFAULT
        ;;
        vzw|vzwpre|comcast|spectrum )
            setprop ro.vendor.mtk_md_sbp_custom_value 12
            setprop ro.vendor.operator.optr OP12
            setprop ro.vendor.operator.spec SPEC0200
            setprop ro.vendor.operator.seg SEGDEFAULT
        ;;
    esac
}

set_cxp_rw_properties ()
{
    optr=`getprop persist.vendor.operator.optr`
    if [ $optr ]; then
        return 0
    fi
	
    case $boot_carrier in
        att|attpre|cricket )
            setprop persist.vendor.operator.optr OP07
            setprop persist.vendor.operator.spec SPEC0407
            setprop persist.vendor.operator.seg SEGDEFAULT
        ;;
        tmo|boost|cc|fi|metropcs|retus|tracfone )
            setprop persist.vendor.operator.optr OP08
            setprop persist.vendor.operator.spec SPEC0200
            setprop persist.vendor.operator.seg SEGDEFAULT
        ;;
        usc )
            setprop persist.vendor.operator.optr OP236
            setprop persist.vendor.operator.spec SPEC0200
            setprop persist.vendor.operator.seg SEGDEFAULT
        ;;
        vzw|vzwpre|comcast|spectrum )
            setprop persist.vendor.operator.optr OP12
            setprop persist.vendor.operator.spec SPEC0200
            setprop persist.vendor.operator.seg SEGDEFAULT
        ;;
    esac
}


set_cxp_readonly_properties
set_cxp_rw_properties
return 0
