## tonga_g-user 11 RRQS31.Q3-68-99-2 25a9a release-keys
- Manufacturer: motorola
- Platform: mt6765
- Codename: tonga
- Brand: motorola
- Flavor: tonga_g-user
- Release Version: 11
- Kernel Version: 4.19.127
- Id: RRQS31.Q3-68-99-2
- Incremental: 25a9a
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Treble Device: true
- Locale: en-US
- Screen Density: 280
- Fingerprint: motorola/tonga_g/tonga:11/RRQS31.Q3-68-99-2/25a9a:user/release-keys
- OTA version: 
- Branch: tonga_g-user-11-RRQS31.Q3-68-99-2-25a9a-release-keys
- Repo: motorola/tonga
